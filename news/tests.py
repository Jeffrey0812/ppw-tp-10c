from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import news, id, sort, search, like, slug
from .models import News


class NewsTest(TestCase):
    def test_news_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_random_url(self):
        response = Client().get('dsadawdwadawdwadawdawd')
        self.assertNotEqual(response.status_code , 200)

    def test_news_using_index_func(self):
        function = resolve('/news/')
        self.assertEqual(function.func, news)
        
    def test_news_particular_url_is_exist(self):
        news = News.objects.create(judul_news = "hhe",desc_news="hehe")
        response = Client().get('/news/'+news.judul_news)
        self.assertEqual(response.status_code, 301)
        
    def test_about_contains_desc(self):
        news = News.objects.create(judul_news = "hhe",desc_news="hehe")
        response = Client().get('/news/')
        response_html = response.content.decode('utf8')
        self.assertContains(response, news.desc_news)
        
    def test_model_can_create_news(self):
        news = News.objects.create(judul_news = "hhe",desc_news="hehe")
        count_all_variable = News.objects.all().count()
        self.assertEqual(count_all_variable, 1)
 
    def test_news_contains_hello(self):
        response = Client().get('/news/')
        response_html = response.content.decode('utf8')
        self.assertContains(response, 'News')

    def test_news_url_from_id(self):
        news = News.objects.create(judul_news = "hhe",desc_news="hehe")
        response = Client().get('/news/id/1')
        self.assertEqual(response.status_code, 301)

    def test_like_url(self):
        news = News.objects.create(judul_news = "hhe",desc_news="hehe")
        response = Client().get('/news/like_news/id/1/')
        self.assertEqual(response.status_code, 200)

    def test_slug_url_exist(self):
        news = News.objects.create(judul_news = "hhe",desc_news="hehe")
        response = Client().get('/news/'+news.judul_news)
        self.assertEqual(response.status_code, 301)

    def test_slug_url_error(self):
        response = Client().get('/news/dsadadwdsad')
        self.assertEqual(response.status_code, 301)

    def test_slug_using_slug_func(self):
        news = News.objects.create(judul_news = "hhe",desc_news="hehe")
        function = resolve('/news/'+news.judul_news+'/')
        self.assertEqual(function.func, slug)

    def test_id_using_id_func(self):
        news = News.objects.create(judul_news = "hhe",desc_news="hehe")
        function = resolve('/news/id/'+str(news.id)+'/')
        self.assertEqual(function.func, id)
