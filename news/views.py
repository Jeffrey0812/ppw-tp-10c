from django.shortcuts import render, redirect, get_object_or_404
from .models import News
from django.http import Http404
from dashboard.forms import SignUp
from .forms import LikeForms
from dashboard.models import User
# Create your views here.
def news(request):
    list_news = News.objects.all().order_by('-date_news')
    list_categories = []
    for news in list_news:
        if news.categories_news not in list_categories:
            list_categories.append(news.categories_news)
    return render(request, 'news.html', {'list_news':list_news,'list_categories':list_categories})

def slug(request, page_slug):
    try:
        news = News.objects.get(judul_news=page_slug)
    except:
        raise Http404('News doesnt exist')
    return render(request, 'id.html', {'news':news})

def id(request, news_id):
    try:
        news = News.objects.get(pk=news_id)
    except:
        raise Http404('News doesnt exist')
    return redirect('/news/'+news.judul_news)

def like(request, news_id):
    news = News.objects.get(id=news_id)
    if (request.method == 'POST'):
        form = LikeForms(request.POST or None)
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        try:
            user = User.objects.get(email=email, password=password, username=username)
        except:
            error_message = "User not found"
            return render(request, 'like_news.html', {'news':news, 'error_message':error_message, 'form':form})
        news.save()
        news.user_like.add(user)
        
        return redirect('/news/'+news.judul_news)
    else:
        form = LikeForms()
    return render(request, 'like_news.html', {'news':news, 'form':form})

def sort(request):
    list_news = News.objects.all().order_by('-categories_news')
    list_categories = []
    for news in list_news:
        if news.categories_news not in list_categories:
            list_categories.append(news.categories_news)
    return render(request, 'news_sort.html', {'list_news':list_news,'list_categories':list_categories})

def search(request):
    query = request.GET['q']
    list_news = News.objects.all()
    news_render = []
    for news in list_news:
        if query in news.judul_news or query in news.desc_news:
            news_render.append(news)
    if news_render == []:
        return render(request, 'search.html' , {'error_message':'Berita tidak ditemukan'})
    list_categories = []
    for news in news_render:
        if news.categories_news not in list_categories:
            list_categories.append(news.categories_news)
    return render(request, 'search.html', {'news':news_render, 'list_categories':list_categories})
