from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', views.ArticlesView.as_view(), name="events"),
    path('<int:events_id>/', views.id, name='events_id'),
    path('register_events/id/<int:events_id>/',
         views.register, name='register_events'),

]
