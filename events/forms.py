from django import forms
from dashboard.models import User


class RegisterEventsForms(forms.ModelForm):
    username = forms.CharField(max_length=30, required=True)
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'type': 'email'}), max_length=254, required=True)
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'type': 'password'}), max_length=8, required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
