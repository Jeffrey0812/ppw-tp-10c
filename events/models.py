from django.db import models
from dashboard.models import User
# Create your models here.


class Events(models.Model):
    judul_events = models.CharField(max_length=100)
    venue_events = models.CharField(max_length=1024, null=True)
    desc_events = models.CharField(max_length=1024)
    date_events = models.DateField(null=True)
    image = models.CharField(max_length=500, null=True)
    participant = models.ManyToManyField(User, null=True)

    class Meta:
        ordering = ['date_events']
