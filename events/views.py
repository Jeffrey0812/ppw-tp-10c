from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Events
from django.http import Http404
from dashboard.forms import SignUp
from .forms import RegisterEventsForms
from dashboard.models import User
# Create your views here.


def id(request, events_id):
    list_events = Events.objects.all().order_by('?')[:3]
    try:
        events = Events.objects.get(pk=events_id)
    except:
        raise Http404('Events doesnt exist')
    return render(request, 'particular.html', {'events': events, 'list_events': list_events})


class ArticlesView(ListView):
    model = Events
    paginate_by = 5
    context_object_name = 'list_events'
    template_name = 'events.html'


def register(request, events_id):
    events = Events.objects.get(id=events_id)
    if (request.method == 'POST'):
        form = RegisterEventsForms(request.POST or None)
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        try:
            user = User.objects.get(
                email=email, password=password, username=username)
        except:
            error_message = "User not found"
            return render(request, 'register_events.html', {'events': events, 'error_message': error_message, 'form': form})
        events.save()
        events.participant.add(user)

        return redirect('/events/' + str(events.id))
    else:
        form = RegisterEventsForms()
    return render(request, 'register_events.html', {'events': events, 'form': form})

