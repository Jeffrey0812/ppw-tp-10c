from django.db import models

# Create your models here.
class User(models.Model):
    birthday = models.DateField()
    live = models.CharField(max_length=40)
    name = models.CharField(max_length=30)
    email = models.EmailField(max_length=254, unique=True)
    password = models.CharField(max_length=8)
    username = models.CharField(max_length=30, unique=True)
    
