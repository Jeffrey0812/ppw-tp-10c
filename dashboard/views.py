from django.shortcuts import render, redirect
from .models import User
from news.models import News
from .forms import SignUp

# Create your views here.
def dashboard(request):
    list_news = News.objects.all()
    return render(request, 'homepage.html', {'news':list_news})


