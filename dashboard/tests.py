from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import dashboard
from django.contrib.auth.models import User
# Create your tests here.

class DashboardTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        
    def test_random_url_is_non_exist(self):
        response = Client().get('STUDENT UNION')
        self.assertNotEqual(response.status_code , 200)
        
    def test_dashboard_using_dashboard_func(self):
        function = resolve('/')
        self.assertEqual(function.func, dashboard)

    def test_login(self):
        user = User.objects.create_user('myemail@test.com', 'bar')
        self.client.login(email='myemail@test.com', password='bar')
        self.assertEquals(self.client.session['_auth_user_id'], 1)

    def test_logout(self):
        self.client = Client()
        self.client.login(username='abc', password="abc")
        response = self.client.get('/admin/')
        self.client.logout()
        self.assertTrue('Log in' in response.content)

        
