from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import about
# Create your tests here.

class AboutUsTest(TestCase):
    def test_aboutus_url_is_exist(self):
        response = Client().get('/aboutus/')
        self.assertEqual(response.status_code, 200)
        
    def test_aboutus_using_about_func(self):
        function = resolve('/aboutus/')
        self.assertEqual(function.func, about)

